package main

import (
	"encoding/json"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"testing"
	"time"

	"gitlab.com/mauromonti/meterusage/meterusagepb"
	"go.nhat.io/grpcmock"
	"go.nhat.io/grpcmock/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func TestWithCORS(t *testing.T) {
	srv := httptest.NewServer(withCORS(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotImplemented)
	})))
	defer srv.Close()

	t.Run("returns CORS headers on OPTION requests", func(t *testing.T) {
		req, err := http.NewRequest(http.MethodOptions, srv.URL, nil)
		if err != nil {
			t.Fatal(err)
		}
		resp, err := http.DefaultClient.Do(req)
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != http.StatusOK {
			t.Errorf("expected status code to be %v, got %v", http.StatusOK, resp.StatusCode)
		}
	})

	t.Run("sets CORS headers before handling requests", func(t *testing.T) {
		resp, err := http.Get(srv.URL)
		if err != nil {
			t.Fatal(err)
		}

		if resp.StatusCode != http.StatusNotImplemented {
			t.Errorf("expected status code to be %v, got %v", http.StatusNotImplemented, resp.StatusCode)
		}
		if resp.Header.Get("Access-Control-Allow-Origin") != "*" {
			t.Errorf("expected Access-Control-Allow-Origin to be *, got %v", resp.Header.Get("Access-Control-Allow-Origin"))
		}
		if resp.Header.Get("Access-Control-Allow-Methods") != "GET, OPTIONS" {
			t.Errorf("expected Access-Control-Allow-Methods to be GET, OPTIONS, got %v", resp.Header.Get("Access-Control-Allow-Methods"))
		}
		if resp.Header.Get("Access-Control-Allow-Headers") == "" {
			t.Error("expected Access-Control-Allow-Headers to be set, got empty value")
		}
	})
}

func TestMeterUsageHandler(t *testing.T) {
	srv := httptest.NewServer(http.HandlerFunc(meterUsageHandler))
	defer srv.Close()

	t.Run("returns 405 on non-GET requests", func(t *testing.T) {
		resp, err := http.Post(srv.URL, "", nil)
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != http.StatusMethodNotAllowed {
			t.Errorf("expected status code to be %v, got %v", http.StatusMethodNotAllowed, resp.StatusCode)
		}
	})

	t.Run("returns 500 on error", func(t *testing.T) {
		resp, err := http.Get(srv.URL)
		if err != nil {
			t.Fatal(err)
		}
		if resp.StatusCode != http.StatusInternalServerError {
			t.Errorf("expected status code to be %v, got %v", http.StatusInternalServerError, resp.StatusCode)
		}
	})

	t.Run("returns data", func(t *testing.T) {
		_, d := grpcmock.MockServerWithBufConn(
			grpcmock.RegisterServiceFromMethods(service.Method{
				ServiceName: "MeterUsageService",
				MethodName:  "ListMeterUsages",
				MethodType:  service.TypeServerStream,
				Input:       &meterusagepb.MeterUsageRequest{},
				Output:      meterusagepb.MeterUsage{},
			}),
			func(s *grpcmock.Server) {
				s.ExpectServerStream("MeterUsageService/ListMeterUsages").
					WithPayload(&meterusagepb.MeterUsageRequest{}).
					ReturnStream().
					Send(&meterusagepb.MeterUsage{Time: time.Date(2019, 1, 1, 0, 15, 0, 0, time.UTC).Format(time.RFC3339), Usage: 55.09}).
					Send(&meterusagepb.MeterUsage{Time: time.Date(2019, 1, 1, 0, 30, 0, 0, time.UTC).Format(time.RFC3339), Usage: 54.64})
			},
		)(t)

		conn, err := grpc.Dial("bufconn", grpc.WithContextDialer(d), grpc.WithTransportCredentials(insecure.NewCredentials()))
		if err != nil {
			t.Fatal(err)
		}
		defer conn.Close()
		grpcClient = meterusagepb.NewMeterUsageServiceClient(conn)

		resp, err := http.Get(srv.URL)
		if err != nil {
			t.Fatal(err)
		}
		defer resp.Body.Close()
		if resp.StatusCode != http.StatusOK {
			t.Errorf("expected status code to be %v, got %v", http.StatusOK, resp.StatusCode)
		}
		body, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			t.Fatal(err)
		}

		var meterUsages []meterusagepb.MeterUsage
		if err := json.Unmarshal(body, &meterUsages); err != nil {
			t.Fatal(err)
		}
		if len(meterUsages) != 2 {
			t.Errorf("expected 2 meter usages, got %v", len(meterUsages))
		}
		if meterUsages[0].Time != time.Date(2019, 1, 1, 0, 15, 0, 0, time.UTC).Format(time.RFC3339) {
			t.Errorf("expected time to be %v, got %v", time.Date(2019, 1, 1, 0, 15, 0, 0, time.UTC).Format(time.RFC3339), meterUsages[0].Time)
		}
		if meterUsages[0].Usage != 55.09 {
			t.Errorf("expected usage to be 55.09, got %v", meterUsages[0].Usage)
		}
		if meterUsages[1].Time != time.Date(2019, 1, 1, 0, 30, 0, 0, time.UTC).Format(time.RFC3339) {
			t.Errorf("expected time to be %v, got %v", time.Date(2019, 1, 1, 0, 30, 0, 0, time.UTC).Format(time.RFC3339), meterUsages[1].Time)
		}
		if meterUsages[1].Usage != 54.64 {
			t.Errorf("expected usage to be 54.64, got %v", meterUsages[1].Usage)
		}
	})
}
