package main

import (
	"errors"
	"log"

	"gitlab.com/mauromonti/meterusage/meterusagepb"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

var grpcConn *grpc.ClientConn
var grpcClient meterusagepb.MeterUsageServiceClient
var errGRPCClientNotInitialized = errors.New("gRPC client not initialized")

func initMeterClient() {
	conn, err := grpc.Dial(serverAddr, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("fail to dial: %v", err)
	}
	grpcClient = meterusagepb.NewMeterUsageServiceClient(conn)
	grpcConn = conn
}

func main() {
	loadEnv()

	initMeterClient()
	defer grpcConn.Close()

	srv := getHTTPServer(listenAddr)
	if err := srv.ListenAndServe(); err != nil {
		log.Fatal(err)
	}
}
