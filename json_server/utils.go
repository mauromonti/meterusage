package main

import (
	"os"
)

var serverAddr, listenAddr string

func loadEnv() {
	serverAddr = os.Getenv("SERVER_ADDR")
	if serverAddr == "" {
		serverAddr = ":8080"
	}
	listenAddr = os.Getenv("LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = ":8081"
	}
}
