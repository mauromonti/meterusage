package main

import (
	"context"
	"encoding/json"
	"io"
	"net/http"
	"time"

	"gitlab.com/mauromonti/meterusage/meterusagepb"
)

func getHTTPServer(addr string) http.Server {
	mux := http.NewServeMux()

	mux.HandleFunc("/meterusage", meterUsageHandler)

	return http.Server{
		Addr:    addr,
		Handler: withCORS(mux),
	}
}

type MeterUsage struct {
	Time  string  `json:"time"`
	Usage float32 `json:"usage"`
}

func listMeterUsages() ([]MeterUsage, error) {
	if grpcClient == nil {
		return nil, errGRPCClientNotInitialized
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*5)
	defer cancel()

	stream, err := grpcClient.ListMeterUsages(ctx, &meterusagepb.MeterUsageRequest{})
	if err != nil {
		return nil, err
	}

	result := make([]MeterUsage, 0)
	for {
		mu, err := stream.Recv()
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, err
		}
		result = append(result, MeterUsage{Time: mu.Time, Usage: mu.Usage})
	}
	return result, nil
}

func meterUsageHandler(w http.ResponseWriter, r *http.Request) {
	if r.Method != http.MethodGet {
		w.WriteHeader(http.StatusMethodNotAllowed)
		return
	}
	meterUsages, err := listMeterUsages()
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	w.Header().Set("Content-Type", "application/json")
	if err := json.NewEncoder(w).Encode(meterUsages); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func withCORS(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Access-Control-Allow-Origin", "*")
		w.Header().Set("Access-Control-Allow-Methods", "GET, OPTIONS")
		w.Header().Set("Access-Control-Allow-Headers", "Accept, Content-Type, Content-Length, Accept-Encoding")
		if r.Method == http.MethodOptions {
			w.WriteHeader(http.StatusOK)
			return
		}
		h.ServeHTTP(w, r)
	})
}
