package main

import (
	"testing"
)

func TestLoadEnv(t *testing.T) {
	t.Run("sets default values if missing", func(t *testing.T) {
		t.Setenv("SERVER_ADDR", "")
		t.Setenv("LISTEN_ADDR", "")
		loadEnv()
		if listenAddr != ":8081" {
			t.Errorf("expected listenAddr to be :8081, got %v", listenAddr)
		}
		if serverAddr != ":8080" {
			t.Errorf("expected serverAddr to be :8080, got %v", serverAddr)
		}
	})
	t.Run("sets listenAddr if present", func(t *testing.T) {
		t.Setenv("LISTEN_ADDR", "localhost:8081")
		loadEnv()
		if listenAddr != "localhost:8081" {
			t.Errorf("expected listenAddr to be localhost:8081, got %v", listenAddr)
		}
	})
	t.Run("sets serverAddr if present", func(t *testing.T) {
		t.Setenv("SERVER_ADDR", "localhost:8080")
		loadEnv()
		if serverAddr != "localhost:8080" {
			t.Errorf("expected serverAddr to be localhost:8080, got %v", serverAddr)
		}
	})
}
