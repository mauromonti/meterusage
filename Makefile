start-db:
	docker-compose -p meterusage --project-directory docker up -d db

start-grpc-server:
	docker-compose -p meterusage --project-directory docker up -d grpc-server

start-json-server:
	docker-compose -p meterusage --project-directory docker up -d json-server

start:
	docker-compose -p meterusage --project-directory docker up -d

run-json-server:
	go run gitlab.com/mauromonti/meterusage/json_server

test:
	go test ./...