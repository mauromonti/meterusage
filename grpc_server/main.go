package main

import (
	"context"
	"log"
	"net"

	"github.com/jackc/pgx/v4"
	"gitlab.com/mauromonti/meterusage/meterusagepb"
	"google.golang.org/grpc"
)

// IPgxConn helps with mocking pgx in tests
type IPgxConn interface {
	Query(context.Context, string, ...interface{}) (pgx.Rows, error)
}

var db IPgxConn

type meterServer struct {
	meterusagepb.UnimplementedMeterUsageServiceServer
}

func (srv *meterServer) ListMeterUsages(req *meterusagepb.MeterUsageRequest, stream meterusagepb.MeterUsageService_ListMeterUsagesServer) error {
	return sendMeterUsages(stream)
}

func main() {
	if err := loadEnv(); err != nil {
		log.Fatal(err)
	}

	// create listener early to fail fast if `listenAddr` is already in use
	listener, err := net.Listen("tcp", listenAddr)
	if err != nil {
		log.Fatal(err)
	}

	log.Println("connecting to database...")
	conn, err := initDB(dbURL)
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close(context.Background())
	db = conn

	// initialize gRPC server
	grpcSrv := grpc.NewServer()
	srv := &meterServer{}
	meterusagepb.RegisterMeterUsageServiceServer(grpcSrv, srv)

	log.Println("listening on", listenAddr)
	if err := grpcSrv.Serve(listener); err != nil {
		log.Fatal(err)
	}
}
