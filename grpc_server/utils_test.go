package main

import (
	"errors"
	"testing"
)

func TestLoadEnv(t *testing.T) {
	t.Run("throws error if DATABASE_URL is missing", func(t *testing.T) {
		t.Setenv("DATABASE_URL", "")
		err := loadEnv()
		if err == nil {
			t.Error("expected error, got nil")
		}
		if !errors.Is(err, errMissingDBURL) {
			t.Errorf("expected error to be %v, got %v", errMissingDBURL, err)
		}
	})
	t.Run("sets default LISTEN_ADDR if missing", func(t *testing.T) {
		t.Setenv("DATABASE_URL", "postgres://user:pass@localhost:5432/db")
		t.Setenv("LISTEN_ADDR", "")
		err := loadEnv()
		if err != nil {
			t.Errorf("expected no error, got %v", err)
		}
		if listenAddr != ":8080" {
			t.Errorf("expected listenAddr to be :8080, got %v", listenAddr)
		}
	})
	t.Run("sets dbURL if present", func(t *testing.T) {
		t.Setenv("DATABASE_URL", "postgres://user:pass@localhost:5432/db")
		err := loadEnv()
		if err != nil {
			t.Errorf("expected no error, got %v", err)
		}
		if dbURL != "postgres://user:pass@localhost:5432/db" {
			t.Errorf("expected dbURL to be postgres://user:pass@localhost:5432/db, got %v", dbURL)
		}
	})
	t.Run("sets listenAddr if present", func(t *testing.T) {
		t.Setenv("DATABASE_URL", "postgres://user:pass@localhost:5432/db")
		t.Setenv("LISTEN_ADDR", "localhost:8080")
		err := loadEnv()
		if err != nil {
			t.Errorf("expected no error, got %v", err)
		}
		if listenAddr != "localhost:8080" {
			t.Errorf("expected listenAddr to be localhost:8080, got %v", listenAddr)
		}
	})
}

func TestInitDB(t *testing.T) {
	t.Run("returns error if connection fails", func(t *testing.T) {
		_, err := initDB("-")
		if err == nil {
			t.Error("expected error, got nil")
		}
	})
	t.Run("sets db if connection succeeds", func(t *testing.T) {
		if err := loadEnv(); err != nil {
			t.Skip(err)
		}
		db, err := initDB("postgres://user:pass@localhost:5432/db")
		if err != nil {
			t.Errorf("expected no error, got %v", err)
		}
		if db == nil {
			t.Error("expected db to be set, got nil")
		}
	})
}
