package main

import (
	"context"
	"math"
	"time"

	"gitlab.com/mauromonti/meterusage/meterusagepb"
)

func sendMeterUsages(stream meterusagepb.MeterUsageService_ListMeterUsagesServer) error {
	rows, err := db.Query(context.Background(), "SELECT measure_time, usage FROM meter_usage")
	if err != nil {
		return err
	}
	defer rows.Close()

	for rows.Next() {
		var t time.Time
		var usage float32
		if err = rows.Scan(&t, &usage); err != nil {
			return err
		}
		if math.IsNaN(float64(usage)) { // filter out possible invalid values
			continue
		}
		if err = stream.Send(&meterusagepb.MeterUsage{
			Time:  t.UTC().Format(time.RFC3339),
			Usage: usage,
		}); err != nil {
			return err
		}
	}
	return nil
}
