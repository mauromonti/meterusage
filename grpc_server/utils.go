package main

import (
	"context"
	"errors"
	"os"

	"github.com/jackc/pgx/v4"
)

var errMissingDBURL = errors.New("missing required DATABASE_URL environment variable")

var dbURL, listenAddr string

func loadEnv() error {
	dbURL = os.Getenv("DATABASE_URL")
	if dbURL == "" {
		return errMissingDBURL
	}
	listenAddr = os.Getenv("LISTEN_ADDR")
	if listenAddr == "" {
		listenAddr = ":8080"
	}
	return nil
}

func initDB(connStr string) (*pgx.Conn, error) {
	conn, err := pgx.Connect(context.Background(), connStr)
	if err != nil {
		return nil, err
	}

	return conn, nil
}
