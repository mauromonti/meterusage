package main

import (
	"context"
	"testing"
	"time"

	"github.com/pashagolub/pgxmock"
	"gitlab.com/mauromonti/meterusage/meterusagepb"
	"google.golang.org/grpc"
)

// Mock stream to collect results from sendMeterUsages
type testStream struct {
	grpc.ServerStream
	Add func(m *meterusagepb.MeterUsage)
}

func (s testStream) Send(m *meterusagepb.MeterUsage) error {
	s.Add(m)
	return nil
}

func TestListMeterUsages(t *testing.T) {
	dbPtr := db
	t.Cleanup(func() {
		db = dbPtr // restore DB connection to be used in other tests
	})

	t.Run("streams correct data", func(t *testing.T) {
		mock, err := pgxmock.NewConn()
		if err != nil {
			t.Fatal(err)
		}
		defer mock.Close(context.Background())
		db = mock

		rows := mock.NewRows([]string{"measure_time", "usage"}).
			AddRow(time.Date(2019, time.January, 1, 0, 15, 0, 0, time.UTC), float32(55.09)).
			AddRow(time.Date(2019, time.January, 1, 0, 30, 0, 0, time.UTC), float32(54.64))
		mock.ExpectQuery("SELECT measure_time, usage FROM meter_usage").WillReturnRows(rows)

		result := make([]*meterusagepb.MeterUsage, 0, 2)
		stream := testStream{Add: func(m *meterusagepb.MeterUsage) {
			result = append(result, m)
		}}
		if err := sendMeterUsages(stream); err != nil {
			t.Fatal(err)
		}
		if len(result) != 2 {
			t.Errorf("expected 2 results, got %d", len(result))
		}
		if result[0].Time != "2019-01-01T00:15:00Z" || result[0].Usage != 55.09 {
			t.Errorf("unexpected result: %+v", result[0])
		}
		if result[1].Time != "2019-01-01T00:30:00Z" || result[1].Usage != 54.64 {
			t.Errorf("unexpected result: %+v", result[1])
		}

		if err := mock.ExpectationsWereMet(); err != nil {
			t.Errorf("unfulfilled expectations: %s", err)
		}
	})
}
