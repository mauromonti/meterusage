CREATE TABLE meter_usage (
    measure_time TIMESTAMP NOT NULL PRIMARY KEY,
    usage NUMERIC(6, 2) NOT NULL
);
SELECT create_hypertable('meter_usage', 'measure_time');

COPY meter_usage (measure_time, usage) FROM '/meterusage.csv' DELIMITER ',' CSV HEADER;
